/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "ast.h"
#include "typeSystem.h"
#include <math.h>

#define U1(val, off) ((u8)((((u64)(s64)(val))&(0xFFul<<((off)<<3ul)))>>((off)<<3ul)))
#define U2(val, off) U1(val, 0ul+off), U1(val, 1ul+off)
#define U4(val, off) U2(val, off), U2(val, off+2ul)
#define U8(val, off) U4(val, off), U4(val, off+4ul)

#define RAX 0u
#define RCX 1u
#define RDX 2u
#define RBX 3u
#define RSP 4u
#define RNONE 4u
#define RBP 5u
#define RSI 6u
#define RDI 7u

#define AM_I0 0u
#define AM_I8 1u
#define AM_I32 2u
#define AM_D 3u

#define C_O 0u
#define C_NO 1u
#define C_B 2u
#define C_C 2u
#define C_NAE 2u
#define C_AE 3u
#define C_NB 3u
#define C_NC 3u
#define C_E 4u
#define C_Z 4u
#define C_NE 5u
#define C_NZ 5u
#define C_BE 6u
#define C_NA 6u
#define C_A 7u
#define C_NBE 7u
#define C_S 8u
#define C_NS 9u
#define C_P 0xAu
#define C_PE 0xAu
#define C_NP 0xBu
#define C_PO 0xBu
#define C_L 0xCu
#define C_NGE 0xCu
#define C_GE 0xDu
#define C_NL 0xDu
#define C_LE 0xEu
#define C_NG 0xEu
#define C_G 0xFu
#define C_NLE 0xFu

#define PUSH_R64(reg) (0x50u|((reg)&7u))
#define POP_R64(reg) (0x58u|((reg)&7u))

#define RM(rAddr, rReg, addrMode) ((((addrMode)&3u)<<6u)|(((rReg)&7u)<<3u)|((rAddr)&7u))
#define MR_SIL0(rAdd, rMul, mulExp, rReg) RM(RNONE, rReg, AM_I0), (((rAdd)&7u)|(((rMul)&7u)<<3u)|(((mulExp)&3u)<<6u))
#define MR_SIL8(rAdd, rMul, mulExp, rReg, imm) RM(RNONE, rReg, AM_I8), (((rAdd)&7u)|(((rMul)&7u)<<3u)|(((mulExp)&3u)<<6u)), U1(imm, 0u)
#define MR_SIL32(rAdd, rMul, mulExp, rReg, imm) RM(RNONE, rReg, AM_I32), (((rAdd)&7u)|(((rMul)&7u)<<3u)|(((mulExp)&3u)<<6u)), U4(imm, 0u)

#define MOV_RR32(dest, source) 0x8B, (0xC0u|(((dest)&7u)<<3u)|((source)&7u))
#define MOV_RR64(dest, source) 0x48, MOV_RR32(dest, source)
#define MOV_RI32(dest, imm) (0xB8u|((dest)&7u)), U4(imm, 0u)
#define MOV_RI64(dest, imm) 0x48, (0xB8u|((dest)&7u)), U8(imm, 0u)
#define MOV_MR_32 0x89
#define MOV_MR_64 0x48, MOV_MR_32
#define MOV_MR_16 0x66, MOV_MR_32
#define MOV_MR_8  0x88
#define MOV_RM_32 0x8B
#define MOV_RM_64 0x48, MOV_RM_32
#define MOV_RM_16 0x66, MOV_RM_32
#define MOV_RM_8  0x8A
#define MOVSX_RM_32 0x48, 0x63
#define MOVSX_RM_16 0x48, 0x0F, 0xBF
#define MOVSX_RM_8  0x48, 0x0F, 0xBE
#define MOV_RBPREL32_R32(offset, reg) MOV_MR_32, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOV_RBPREL32_R64(offset, reg) MOV_MR_64, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOV_RBPREL32_R16(offset, reg) MOV_MR_16, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOV_RBPREL32_R8(offset, reg)  MOV_MR_8, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOV_R32_RBPREL32(reg, offset) MOV_RM_32, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOV_R64_RBPREL32(reg, offset) MOV_RM_64, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOV_R16_RBPREL32(reg, offset) MOV_RM_16, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOV_R8_RBPREL32(reg, offset)  MOV_RM_8, MR_SIL32(RBP, RNONE, 0u, reg, offset)
#define MOVSX_R64_RBPREL32_32(reg, offset) 0x48, 0x63, (0x84u|(((reg)&7u)<<3u)), 0xE5, U4(offset, 0u)
#define MOVSX_R64_RBPREL32_16(reg, offset) 0x48, 0x0F, 0xBF, (0x84u|(((reg)&7u)<<3u)), 0xE5, U4(offset, 0u)
#define MOVSX_R64_RBPREL32_8(reg, offset)  0x48, 0x0F, 0xBE, (0x84u|(((reg)&7u)<<3u)), 0xE5, U4(offset, 0u)

#define LEA32 0x8D
#define LEA64 0x48, LEA32

#define SUB_R32_I32(reg, imm) 0x81, (0xE8u|((reg)&7u)), U4(imm, 0u)
#define SUB_R64_I32(reg, imm) 0x48, SUB_R32_I32(reg, imm)
#define ADD_R32_I32(reg, imm) 0x81, (0xC0u|((reg)&7u)), U4(imm, 0u)
#define ADD_R64_I32(reg, imm) 0x48, ADD_R32_I32(reg, imm)
#define ADD_RR32(r1, r2) 0x01, (0xC0u|(((r2)&7u)<<3u)|((r1)&7u))
#define ADD_RR64(r1, r2) 0x48, ADD_RR32(r1, r2)
#define SUB_RR32(r1, r2) 0x29, (0xC0u|(((r2)&7u)<<3u)|((r1)&7u))
#define SUB_RR64(r1, r2) 0x48, SUB_RR32(r1, r2)
#define NEG_R32(reg) 0xF7, (0xD8u|((reg)&7u))
#define NEG_R64(reg) 0x48, NEG_R32(reg)
#define MUL_R32(reg) 0xF7, (0xE0u|((reg)&7u))
#define MUL_R64(reg) 0x48, MUL_R32(reg)
#define DIVMOD_R32(reg) 0xF7, (0xF0u|((reg)&7u))
#define DIVMOD_R64(reg) 0x48, DIVMOD_R32(reg)
#define SHR_R32(dest) 0xD3, (0xE8u|((dest)&7u))
#define SHR_R64(dest) 0x48, SHR_R32(dest)
#define SHL_R32(dest) 0xD3, (0xE0u|((dest)&7u))
#define SHL_R64(dest) 0x48, SHL_R32(dest)
#define SHL_R32_I8(dest, imm) 0xC1, RM(dest, 4u, AM_D), U1(imm, 0u)
#define SHL_R64_I8(dest, imm) 0x48, SHL_R32_I8(dest, imm)
#define OR_RR32(r1, r2) 0x09, (0xC0u|(((r2)&7u)<<3u)|((r1)&7u))
#define OR_RR64(r1, r2) 0x48, OR_RR32(r1, r2)
#define AND_RR32(r1, r2) 0x21, (0xC0u|(((r2)&7u)<<3u)|((r1)&7u))
#define AND_RR64(r1, r2) 0x48, AND_RR32(r1, r2)
#define XOR_RR32(r1, r2) 0x31, (0xC0u|(((r2)&7u)<<3u)|((r1)&7u))
#define XOR_RR64(r1, r2) 0x48, XOR_RR32(r1, r2)
#define NOT_R32(reg) 0xF7, (0xD0u|((reg)&7u))
#define NOT_R64(reg) 0x48, NOT_R32(reg)
#define INC_R32(reg) 0xFF, RM(reg, 0u, AM_D)
#define INC_R64(reg) 0x48, INC_R32(reg)

#define ADD_RM32 0x03
#define ADD_RM64 0x48, ADD_RM32

#define RET_NEAR 0xC3
#define CALL_NEAR(offset) 0xE8, U4(offset, 0u)
#define SYSCALL(number) MOV_RI32(RAX, (u32)number), 0x0F, 0x05
#define JMP_SHORT(offset) 0xEB, U1(offset, 0u)
#define JCC_SHORT(cond, offset) (0x70u|(cond)), U1(offset, 0u)

#define TEST_RR32(r1, r2) 0x85, RM(r1, r2, AM_D)
#define TEST_RR64(r1, r2) 0x48, TEST_RR32(r1, r2)
#define TEST_RR16(r1, r2) 0x66, TEST_RR32(r1, r2)
#define TEST_RR8(r1, r2) 0x84, RM(r1, r2, AM_D)

#define OPCS_LEN(args...) (sizeof((u8[]){0, ##args})-1u)

namespace wheelie {
    namespace ast {

        void emitDerefRAX(std::vector<u8> &buffer, typeSystem::typeDesc *type) {
            const typeSystem::typeData *t = type->resolved;
            bool isSigned = t == &typeSystem::builtin::signed8 || t == &typeSystem::builtin::signed16 ||
                            t == &typeSystem::builtin::signed32 || t == &typeSystem::builtin::signed64;
            switch (type->resolved->size) {
                case 1:
                    if (isSigned)
                        buffer.insert(buffer.end(), {MOVSX_RM_8, RM(RAX, RAX, AM_I0), PUSH_R64(RAX)});
                    else
                        buffer.insert(buffer.end(), {XOR_RR64(RBX, RBX), MOV_RM_8, RM(RAX, RBX, AM_I0), PUSH_R64(RBX)});
                    break;
                case 2:
                    if (isSigned)
                        buffer.insert(buffer.end(), {MOVSX_RM_16, RM(RAX, RAX, AM_I0), PUSH_R64(RAX)});
                    else
                        buffer.insert(buffer.end(),
                                      {XOR_RR64(RBX, RBX), MOV_RM_16, RM(RAX, RBX, AM_I0), PUSH_R64(RBX)});
                    break;
                case 4:
                    if (isSigned)
                        buffer.insert(buffer.end(), {MOVSX_RM_32, RM(RAX, RAX, AM_I0), PUSH_R64(RAX)});
                    else
                        buffer.insert(buffer.end(), {MOV_RM_32, RM(RAX, RAX, AM_I0), PUSH_R64(RAX)});
                    break;
                case 8:
                    buffer.insert(buffer.end(), {MOV_RM_64, RM(RAX, RAX, AM_I0), PUSH_R64(RAX)});
                    break;
                default:
                    buffer.insert(buffer.end(), {PUSH_R64(RAX)});
                    break;
            }
        }

        void ASTNodeFunction::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            ctx.symbols[&this->declSelf] = buffer.size();
            s64 cntr = 16;
            for (auto &arg : this->args) {
                ctx.symbols[&arg] = cntr;
                cntr += std::max((u64) 8,
                                 arg.type->getSize()); //Size will be a multiple of 8 to preserve alignment (if > 8)
            }
            ctx.curFunc = this;
            buffer.insert(buffer.end(), {PUSH_R64(RBP), MOV_RR64(RBP, RSP), SUB_R64_I32(RSP, 0u)});
            auto placeToInsert = buffer.size() - 4;
            ctx.localOffset = 0;
            ASTNodeWide::emit(buffer, ctx);
            for (int i = 0; i < 4; i++) {
                buffer[placeToInsert + i] = (ctx.localOffset & 0xFFu);
                ctx.localOffset >>= 8u;
            }
        }

        void ASTNodeWide::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            for (auto node : children) {
                node->emit(buffer, ctx);
            }
        }

        void ASTNodeRoot::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            if (fileId == 0) {
                ASTNodeFunction *mainFunc = nullptr;
                if (!children.empty()) {
                    for (ASTNode *pNode : dynamic_cast<ASTNodeRoot *>(children.front())->children) {
                        if (pNode->type != Function) continue;
                        auto *nodeFunc = dynamic_cast<ASTNodeFunction *>(pNode);
                        if (nodeFunc->name.value == "main") {
                            mainFunc = nodeFunc;
                            break;
                        }
                    }
                }
                if (!mainFunc) {
                    printer::PrinterManager::errPrinter.printLn("error: Entry point not found in given file!");
                    ctx.success = false;
                    return;
                }
                if (!mainFunc->args.empty()) {
                    bool valid = true;
                    if (mainFunc->args.size() == 1) {
                        const parser::declData &arg = mainFunc->args[0];
                        if (!arg.type->arrayDesc || !arg.type->arrayDesc->arrayDesc ||
                            arg.type->arrayDesc->arrayDesc->arrayDesc ||
                            arg.type->arrayDesc->arrayDesc->getSize() != 1) {
                            valid = false;
                        } else {
                            buffer.insert(buffer.end(),
                                          {
                                                  MOV_RR64(RBP, RSP),
                                                  SUB_R64_I32(RSP, 16u),
                                                  MOV_RR64(RCX, RSP),
                                                  MOV_RM_64, RM(RBP, RAX, AM_I8), U1(0, 0u),
                                                  MOV_MR_64, RM(RBP, RAX, AM_I8), U1(-8, 0u),
                                                  SHL_R64_I8(RAX, 4u),
                                                  SUB_RR64(RSP, RAX),
                                                  MOV_MR_64, RM(RBP, RSP, AM_I8), U1(-16, 0u),
                                                  LEA64, RM(RBP, RSI, AM_I8), U1(8, 0u),
                                                  MOV_RR64(RDI, RSP),
                                                  PUSH_R64(RCX),
                                                  MOV_RM_64, RM(RSI, RAX, AM_I0), //3
                                                  TEST_RR64(RAX, RAX), //3
                                                  JCC_SHORT(C_Z, 48), //2
                                                  MOV_MR_64, RM(RDI, RAX, AM_I0), //3
                                                  MOV_RR64(RBX, RAX), //3
                                                  MOV_RR64(RCX, RAX), //3
                                                  MOV_RM_8, RM(RBX, RAX, AM_I0), //2
                                                  INC_R64(RBX), //3
                                                  TEST_RR8(RAX, RAX), //2
                                                  JCC_SHORT(C_NZ, -9), //2
                                                  SUB_RR64(RBX, RCX), //3
                                                  SUB_R64_I32(RBX, 1), //7
                                                  MOV_MR_64, RM(RDI, RBX, AM_I8), U1(8, 0u), //4
                                                  ADD_R64_I32(RDI, 16), //7
                                                  ADD_R64_I32(RSI, 8), //7
                                                  JMP_SHORT(-56) //2
                                          });
                        }
                    } else
                        valid = false;
                    if (!valid) {
                        printer::PrinterManager::errPrinter.printLn("error: Entry point has wrong type signature!");
                        ctx.success = false;
                        return;
                    }
                }
                buffer.insert(buffer.end(), {CALL_NEAR(0u), MOV_RR64(RDI, RAX), SYSCALL(60)});
                int offset = buffer.size() - OPCS_LEN(MOV_RR64(RDI, RAX), SYSCALL(60));
                ctx.placeholders.emplace_back(offset - 4, &mainFunc->declSelf, -offset, 0, 4);
            }
            ASTNodeWide::emit(buffer, ctx);
        }

        bool emitBuiltinOperator(std::vector<u8> &buffer, const ASTNodeOperator *op) {
            if (op->resolved == &typeSystem::builtin::addNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), ADD_RR64(RAX, RCX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::subNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), SUB_RR64(RAX, RCX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::negNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), NEG_R64(RAX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::mulNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), MUL_R64(RCX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::divNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), DIVMOD_R64(RCX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::modNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), DIVMOD_R64(RCX), PUSH_R64(RDX)});
            } else if (op->resolved == &typeSystem::builtin::shrNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), SHR_R64(RAX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::shlNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), SHL_R64(RAX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::orNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), OR_RR64(RAX, RCX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::andNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), AND_RR64(RAX, RCX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::xorNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), POP_R64(RCX), XOR_RR64(RAX, RCX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::notNum) {
                buffer.insert(buffer.end(), {POP_R64(RAX), NOT_R64(RAX), PUSH_R64(RAX)});
            } else if (op->resolved == &typeSystem::builtin::arrIndex) {
                typeSystem::typeDesc *arrType = typeSystem::getType(op->left)->arrayDesc;
                buffer.insert(buffer.end(), {
                        POP_R64(RCX),
                        POP_R64(RAX),
                        MOV_RI32(RBX, arrType->resolved->size),
                        MUL_R64(RBX),
                        ADD_RM64, RM(RCX, RAX, AM_I0),
                });
                emitDerefRAX(buffer, arrType);
            } else {
                return false;
            }
            return true;
        }

        void ASTNodeOperator::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            if (right) right->emit(buffer, ctx);
            if (left) left->emit(buffer, ctx);
            if (emitBuiltinOperator(buffer, this)) return;
            const locationData &ld = locData;
            printer::PrinterManager::errPrinter.printLn(
                    "%s:%d:%d: error: Internal compiler error! Operator not found!",
                    ld.file.c_str(), ld.line, ld.column);
            ctx.success = false;
        }

        void ASTNodeNumber::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            buffer.insert(buffer.end(), {MOV_RI64(RAX, value.integral), PUSH_R64(RAX)});
        }

        void ASTNodeReturn::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            if (ctx.curFunc->retType->resolved != &typeSystem::builtin::voidType &&
                ctx.curFunc->retType->resolved->size <= 8) {
                returnValue->emit(buffer, ctx);
                buffer.push_back(POP_R64(RAX));
            }
            if (ctx.localOffset != 0)
                buffer.insert(buffer.end(), {ADD_R64_I32(RSP, ctx.localOffset)});
            buffer.insert(buffer.end(), {POP_R64(RBP), RET_NEAR});
        }

        void ASTNodeSymbolUse::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            if (resolved->type->resolved->size <= 8) {
                if (resolved->onStack) {
                    const typeSystem::typeData *t = resolved->type->resolved;
                    bool isSigned = t == &typeSystem::builtin::signed8 || t == &typeSystem::builtin::signed16 ||
                                    t == &typeSystem::builtin::signed32 || t == &typeSystem::builtin::signed64;
                    switch (resolved->type->resolved->size) {
                        case 1:
                            if (isSigned)
                                buffer.insert(buffer.end(), {MOVSX_R64_RBPREL32_8(RAX, 0u), PUSH_R64(RAX)});
                            else
                                buffer.insert(buffer.end(),
                                              {XOR_RR64(RAX, RAX), MOV_R8_RBPREL32(RAX, 0u), PUSH_R64(RAX)});
                            break;
                        case 2:
                            if (isSigned)
                                buffer.insert(buffer.end(), {MOVSX_R64_RBPREL32_16(RAX, 0u), PUSH_R64(RAX)});
                            else
                                buffer.insert(buffer.end(),
                                              {XOR_RR64(RAX, RAX), MOV_R16_RBPREL32(RAX, 0u), PUSH_R64(RAX)});
                            break;
                        case 4:
                            if (isSigned)
                                buffer.insert(buffer.end(), {MOVSX_R64_RBPREL32_32(RAX, 0u), PUSH_R64(RAX)});
                            else
                                buffer.insert(buffer.end(), {MOV_R32_RBPREL32(RAX, 0u), PUSH_R64(RAX)});
                            break;
                        case 8:
                            buffer.insert(buffer.end(), {MOV_R64_RBPREL32(RAX, 0u), PUSH_R64(RAX)});
                            break;
                    }
                    int offset = OPCS_LEN(PUSH_R64(RAX)) + 4;
                    ctx.placeholders.emplace_back(buffer.size() - offset, resolved, 0, 0, 4);
                } else {
                    const locationData &ld = name.locData;
                    printer::PrinterManager::errPrinter.printLn(
                            "%s:%d:%d: error: Usage of global symbols currently not supported!",
                            ld.file.c_str(), ld.line, ld.column,
                            name.value.c_str());
                    ctx.success = false;
                }
            } else {
                //Passed by pointer
                buffer.insert(buffer.end(), {MOV_R64_RBPREL32(RAX, 0u), PUSH_R64(RAX)});
                int offset = OPCS_LEN(PUSH_R64(RAX)) + 4;
                ctx.placeholders.emplace_back(buffer.size() - offset, resolved, 0, 0, 4);
            }
        }

        static bool emitSymbolAssignmentStack(std::vector<u8> &buffer, s32 offset, const typeSystem::typeDesc *type) {
            switch (type->getSize()) {
                case 1:
                    buffer.insert(buffer.end(), {POP_R64(RAX), MOV_RBPREL32_R8((u32) offset, RAX)});
                    break;
                case 2:
                    buffer.insert(buffer.end(), {POP_R64(RAX), MOV_RBPREL32_R16((u32) offset, RAX)});
                    break;
                case 4:
                    buffer.insert(buffer.end(), {POP_R64(RAX), MOV_RBPREL32_R32((u32) offset, RAX)});
                    break;
                case 8:
                    buffer.insert(buffer.end(), {POP_R64(RAX), MOV_RBPREL32_R64((u32) offset, RAX)});
                    break;
                default: {
                    const locationData &ld = type->locData;
                    printer::PrinterManager::errPrinter.printLn(
                            "%s:%d:%d: error: Invalid size for assignment: type:'%s' of size %d",
                            ld.file.c_str(), ld.line, ld.column, type->name.c_str(), type->getSize());
                    return false;
                }
            }
            return true;
        }

        void ASTNodeAssignment::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            value->emit(buffer, ctx);
            if (resolved->onStack) {
                ctx.success &= emitSymbolAssignmentStack(buffer, ctx.symbols[resolved], resolved->type);
            }
        }

        void ASTNodeDeclaration::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            if (value) value->emit(buffer, ctx);
            if (data.onStack) {
                ctx.localOffset += std::max((u64) 8, data.type->getSize());
                if (value) {
                    ctx.success &= emitSymbolAssignmentStack(buffer, -ctx.localOffset, data.type);
                }
                ctx.symbols[&this->data] = -ctx.localOffset;
            }
        }

        void ASTNodeDot::emit(std::vector<u8> &buffer, emitContext &ctx) const {
            parent->emit(buffer, ctx);
            buffer.insert(buffer.end(),
                          {POP_R64(RBX), MOV_RM_64, MR_SIL32(RBX, RNONE, 0u, RAX, offset), PUSH_R64(RAX)});
        }
    }
}
