/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef BOOTSTRAP_AST_H
#define BOOTSTRAP_AST_H

#include <vector>
#include <iostream>
#include <map>

#include "types.h"
#include "parser.h"
#include "printer.h"

namespace wheelie {
    namespace ast {

        class ASTNode;

        struct emitPlaceholder {
            u64 addr{};
            const parser::declData *toFill{};
            s64 toAdd{};
            s8 shift{};
            u8 size{};

            emitPlaceholder(u64 addr, const parser::declData *toFill, s64 toAdd, s8 shift, u8 size) : addr(addr),
                                                                                                      toFill(toFill),
                                                                                                      toAdd(toAdd),
                                                                                                      shift(shift),
                                                                                                      size(size) {}
        };

        class ASTNodeFunction;

        struct emitContext {
            std::vector<emitPlaceholder> placeholders{};
            std::map<const parser::declData *, u64> symbols{};
            const ASTNodeFunction *curFunc{};
            u64 localOffset{};
            bool success{true};
        };

        enum ASTNodeType {
            None, Root, Function, Return, Wide, Operator, NumConstant, SymbolUse, Declaration, Assignment, Dot
        };

        class ASTNode {
        public:
            ASTNodeType type;

            virtual void emit(std::vector<u8> &buffer, emitContext &ctx) const = 0;

            virtual void print(printer::Printer &printer) const = 0;

            ASTNode() : type(None) {}

            virtual ~ASTNode() = default;
        };

        class ASTNodeWide : public ASTNode {
        public:
            std::vector<ASTNode *> children{};

            ASTNodeWide() { type = Wide; }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

            ~ASTNodeWide() override {
                for (ASTNode *node : children) {
                    delete node;
                }
            }
        };

        typedef std::pair<std::string, typeSystem::typeData *> typePair;

        class ASTNodeRoot : public ASTNodeWide {
        public:
            const u32 fileId;
            std::map<std::string, typeSystem::typeData *> types;
            std::map<std::string, parser::operatorData *> operators;

            explicit ASTNodeRoot(const u32 fileId) : fileId(fileId) { type = Root; }

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.printLn(printer::PrinterManager::files[fileId].c_str());
                printer.depth++;
                for (ASTNode *node : children) {
                    node->print(printer);
                }
                printer.depth--;
            }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;
        };

        class ASTNodeDeclaration : public ASTNode {
        public:
            parser::declData data;
            ASTNode *value{};

            ASTNodeDeclaration(const parser::Token &name, typeSystem::typeDesc *typeDesc, bool onStack) : data(typeDesc,
                                                                                                               name,
                                                                                                               onStack) { type = Declaration; }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.print("declare '%s' of type '", data.name.value.c_str());
                data.type->print(printer);
                printer.printLn("'");
                if (value) {
                    printer.depth++;
                    value->print(printer);
                    printer.depth--;
                }
            }
        };

        class ASTNodeAssignment : public ASTNode {
        public:
            parser::Token name;
            const parser::declData *resolved{};
            ASTNode *value;

            ASTNodeAssignment(const parser::Token &name, ASTNode *value) : name(name),
                                                                           value(value) { type = Assignment; }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.printLn("assign '%s'", name.value.c_str());
                printer.depth++;
                if (value) value->print(printer);
                else {
                    printer.printPre();
                    printer.printLn("#INV");
                }
                printer.depth--;
            }
        };

        class ASTNodeOperator : public ASTNode {
        public:
            ASTNode *left{}, *right{};
            std::string op;
            locationData locData;
            typeSystem::typeDesc retType{};
            parser::operatorData *resolved{};

            explicit ASTNodeOperator(const parser::Token &t) : op(t.value), locData(t.locData) { type = Operator; };

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.printLn("%s (%sfix)", op.c_str(),
                                (left == nullptr ? "pre" : (right == nullptr ? "post" : "in")));
                printer.depth++;
                if (left) left->print(printer);
                if (right) right->print(printer);
                printer.depth--;
            }

            ~ASTNodeOperator() override {
                delete left;
                delete right;
            }
        };

        class ASTNodeNumber : public ASTNode {
        public:
            union {
                u64 integral;
                double floating;
            } value{};
            bool isFloat;

            ASTNodeNumber(const u64 u64, bool isFloat) : isFloat(isFloat) {
                value.integral = u64;
                type = NumConstant;
            }

            explicit ASTNodeNumber(const double f64) {
                value.floating = f64;
                isFloat = true;
            }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

            void print(printer::Printer &printer) const override {
                printer.printPre();
                if (isFloat) {
                    printer.printLn("%f", value.floating);
                } else {
                    printer.printLn("%d", value.integral);
                }
            }
        };

        class ASTNodeReturn : public ASTNode {
        public:
            ASTNode *returnValue{};

            ASTNodeReturn() { type = Return; };

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.printLn("return");
                if (returnValue) {
                    printer.depth++;
                    returnValue->print(printer);
                    printer.depth--;
                }
            }

            ~ASTNodeReturn() override {
                delete returnValue;
            }
        };

        class ASTNodeFunction : public ASTNodeWide {
        public:
            const parser::Token name;
            typeSystem::typeDesc *retType;
            std::vector<parser::declData> args;
            parser::declData declSelf;

            explicit ASTNodeFunction(const parser::Token &name) : name(name), retType(),
                                                                  declSelf(new typeSystem::typeDesc("function",
                                                                                                    &typeSystem::builtin::funcType),
                                                                           name, false) { type = Function; }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.print("function '%s' returning '", name.value.c_str());
                retType->print(printer);
                printer.printLn("'");
                printer.depth++;
                for (const parser::declData &d : args) {
                    printer.printPre();
                    printer.print("argdecl '%s' of type '", d.name.value.c_str());
                    d.type->print(printer);
                    printer.printLn("'");
                }
                for (ASTNode *node : children) {
                    node->print(printer);
                }
                printer.depth--;
            }

            ~ASTNodeFunction() override {
                delete retType;
            }
        };

        class ASTNodeSymbolUse : public ASTNode {
        public:
            parser::Token name;
            const parser::declData *resolved{};

            explicit ASTNodeSymbolUse(const parser::Token &t) : name(t) { type = SymbolUse; }

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.print("Symbol '%s' of type '", name.value.c_str());
                resolved->type->print(printer);
                printer.printLn("'");
            }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;

        };

        class ASTNodeDot : public ASTNode {
        public:
            ASTNode *parent;
            parser::Token member;
            int offset{-1};
            typeSystem::typeDesc *resolvedType{};

            explicit ASTNodeDot(ASTNode *parent, const parser::Token &member) : parent(parent),
                                                                                member(member) { type = Dot; }

            void print(printer::Printer &printer) const override {
                printer.printPre();
                printer.print("Member '%s' of type '", member.value.c_str());
                resolvedType->print(printer);
                printer.printLn("' of:");
                printer.depth++;
                parent->print(printer);
                printer.depth--;
            }

            void emit(std::vector<u8> &buffer, emitContext &ctx) const override;
        };

    }
}

#endif //BOOTSTRAP_AST_H
