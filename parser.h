
/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef BOOTSTRAP_PARSER_H
#define BOOTSTRAP_PARSER_H

#include <string>
#include "types.h"
#include "typeSystem.h"

namespace wheelie { namespace ast { class ASTNodeRoot; }}
namespace wheelie {
    namespace parser {


        struct Token {
        public:
            locationData locData;
            std::string value;
            TokenTag tag = General;
            u8 metadata{};

            explicit Token(const std::string &file) : locData(file) {}

            Token(const Token &other) = default;
        };

        extern void operator+=(Token &t1, const Token &t2);

        struct declData {
            typeSystem::typeDesc *type;
            Token name;
            bool onStack{};

            declData(typeSystem::typeDesc *type, const Token &name, bool onStack) : type(type), name(name),
                                                                                    onStack(onStack) {}

            ~declData() noexcept {
                delete type;
            }

            declData(declData &&old) noexcept : type(old.type), name(old.name), onStack(old.onStack) {
                old.type = nullptr;
            }

            declData(const declData &) = default;

            declData &operator=(declData const &) = default;
        };

        struct operatorData {
            declData declSelf{nullptr, Token(""), false};
            typeSystem::typeDesc *returnType, *leftType, *rightType;

            explicit operatorData(typeSystem::typeDesc *returnType, typeSystem::typeDesc *leftType,
                                  typeSystem::typeDesc *rightType) : returnType(returnType), leftType(leftType),
                                                                     rightType(rightType) {}

            ~operatorData() {
                delete returnType;
            }
        };

        extern ast::ASTNodeRoot *parse(const std::string &path);

    }
}

#endif //BOOTSTRAP_PARSER_H
