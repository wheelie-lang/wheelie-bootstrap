/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "elfWriter.h"

#include <fstream>
#include <sys/stat.h>

namespace wheelie {
    void ElfWriter::write(const ast::ASTNodeRoot *root, const char *path) {
        std::vector<u8> buffer;

        buffer.insert(buffer.end(), {0x7F, 'E', 'L', 'F', 2, 1, 1, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0}); // Id Header
        buffer.insert(buffer.end(), {3, 0, 62, 0, 1, 0, 0, 0,
                                     0, 0x10, 0, 0, 0, 0, 0, 0,
                                     0x40, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0x40, 0, 0x38, 0, 3, 0, 0x40, 0, 0, 0, 0, 0}); // Header end
        buffer.insert(buffer.end(), {6, 0, 0, 0, 4, 0, 0, 0, 0x40, 0, 0, 0, 0, 0, 0, 0,
                                     0x40, 0, 0, 0, 0, 0, 0, 0, 0x40, 0, 0, 0, 0, 0, 0, 0,
                                     0xA8, 0, 0, 0, 0, 0, 0, 0, 0xA8, 0, 0, 0, 0, 0, 0, 0,
                                     8, 0, 0, 0, 0, 0, 0, 0}); // PHDR
        buffer.insert(buffer.end(), {1, 0, 0, 0, 4, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                                     0xE8, 0, 0, 0, 0, 0, 0, 0, 0xE8, 0, 0, 0, 0, 0, 0, 0,
                                     8, 0, 0, 0, 0, 0, 0, 0}); // LOAD (Header)
        buffer.insert(buffer.end(), {1, 0, 0, 0, 4u | 1u, 0, 0, 0, 0, 0x10, 0, 0, 0, 0, 0, 0,
                                     0, 0x10, 0, 0, 0, 0, 0, 0, 0, 0x10, 0, 0, 0, 0, 0, 0,
                                     0x70, 0, 0, 0, 0, 0, 0, 0, 0x70, 0, 0, 0, 0, 0, 0, 0,
                                     0, 0x10, 0, 0, 0, 0, 0, 0}); // LOAD (Code)
        buffer.resize(0x1000);

        ast::emitContext ctx;
        root->emit(buffer, ctx);

        u64 size = buffer.size() - 0x1000;
        if (size == 0 || !ctx.success) return;
        for (const auto &placeholder : ctx.placeholders) {
            u64 toWrite = ctx.symbols[placeholder.toFill];
            if (placeholder.shift < 0) toWrite >>= (u8) -placeholder.shift;
            else toWrite <<= (u8) placeholder.shift;
            toWrite += placeholder.toAdd;
            for (u64 idx = placeholder.addr, shift = 0; idx < placeholder.addr + placeholder.size; idx++, shift += 8) {
                buffer[idx] = ((toWrite >> shift) & 0xFFu);
            }
        }
        for (u8 i = 0, j = 0; i < 8; i++, j += 8) {
            buffer[0xB0 + 0x20 + i] = (size >> j) & 0xFFu;
            buffer[0xB0 + 0x28 + i] = (size >> j) & 0xFFu;
        }

        std::ofstream ofs(path, std::ios::binary);
        ofs.write((const char *) buffer.begin().base(), buffer.size());
        ofs.close();
        chmod(path, S_IRWXU | S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH);
    }
}
