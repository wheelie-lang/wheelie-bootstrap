/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "printer.h"

#include <fstream>
#include <memory>
#include <stdarg.h>

#include "types.h"

namespace wheelie {
    namespace printer {
        std::vector<std::string> PrinterManager::files;
        Printer PrinterManager::errPrinter = Printer(nullptr, "\t", "\t", "\033[31m");
        Printer PrinterManager::infoPrinter = Printer(nullptr, "\t", "\t", "\033[32m");
        Printer PrinterManager::warnPrinter = Printer(nullptr, "\t", "\t", "\033[33m");

        static char buf[1024];
        static std::unique_ptr<char[]> heapBuf;

        Printer PrinterManager::filePrinter(const std::string &path,
                                            const char *finalPadding,
                                            const char *padding,
                                            const char *style) {
            auto *file = new std::ofstream(path);
            return Printer(file, finalPadding, padding, style);
        }

        void Printer::printPre() const {
            if (depth) {
                u16 d2 = depth;
                while (--d2) {
                    *out << padding;
                }
                *out << finalPadding;
            }
        }

        void Printer::print(const char *fmt, ...) const {
            va_list args;
            va_start(args, fmt);
            this->vprint(fmt, args);
            va_end(args);
        }

        void Printer::vprint(const char *fmt, va_list args) const {
            *out << this->style;

            va_list cpy;
            va_copy(cpy, args);
            s64 result = vsnprintf(buf, sizeof(buf), fmt, args);
            if (result < 0) {
                va_end(cpy);
                return;
            } else if (static_cast<u64>(result) < sizeof(buf)) {
                out->write(buf, result);
            } else {
                heapBuf.reset(new char[result + 1]);
                result = vsnprintf(heapBuf.get(), (size_t) (result + 1), fmt, cpy);
                if (result > 0) {
                    out->write(heapBuf.get(), result);
                }
            }
            va_end(cpy);
            *out << "\033[m";
        }

        void Printer::printLn(const char *fmt, ...) const {
            va_list args;
            va_start(args, fmt);
            this->vprint(fmt, args);
            va_end(args);
            out->write("\n", 1);
        }
    }
}
