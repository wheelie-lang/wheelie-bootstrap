/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef BOOTSTRAP_PRINTER_H
#define BOOTSTRAP_PRINTER_H

#include <string>
#include <iostream>
#include <vector>

#include "types.h"

namespace wheelie {
    namespace printer {

        class Printer {
            const char *padding, *finalPadding, *style;

            void vprint(const char *fmt, va_list args) const;

        public:
            u16 depth{};
            std::ostream *out;

            explicit Printer(std::ostream *out,
                             const char *finalPadding = "\t",
                             const char *padding = "\t",
                             const char *style = "") noexcept :
                    padding(padding), finalPadding(finalPadding), style(style), out(out) {}

            void printPre() const;

            void printLn(const char *fmt, ...) const;

            void print(const char *fmt, ...) const;

            ~Printer() {
                if (out != &std::cout && out != &std::cerr)
                    delete out;
            }

        };

        class PrinterManager {

        public:
            static std::vector<std::string> files;
            static Printer errPrinter;
            static Printer warnPrinter;
            static Printer infoPrinter;

            static Printer stdPrinter(const char *finalPadding = "\t",
                                      const char *padding = "\t",
                                      const char *style = "") {
                return Printer(&std::cout, finalPadding, padding, style);
            }

            static Printer filePrinter(const std::string &path,
                                       const char *finalPadding = "\t",
                                       const char *padding = "\t",
                                       const char *style = "");
        };
    }
}


#endif //BOOTSTRAP_PRINTER_H
