/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef BOOTSTRAP_TYPES_H
#define BOOTSTRAP_TYPES_H

#include <stdint.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

enum TokenTag {
    General, Number, Text, Operator, Delimiter
};

struct locationData {
public:
    const std::string &file;
    int line{}, column{};

    explicit locationData(const std::string &file) : file(file) {}

    locationData(const locationData &other) = default;

    locationData &operator=(const locationData &other) {
        line = other.line;
        column = other.column;
        return *this;
    }
};

#endif //BOOTSTRAP_TYPES_H
