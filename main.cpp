/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <iostream>
#include <string>

#include "ast.h"
#include "parser.h"
#include "elfWriter.h"

using namespace wheelie;

int main(int argc, const char **argv) {
    if (argc < 2) {
        std::cout << "Expected an argument!\nUsage: bootstrap <main.wls file>" << std::endl;
        return 1;
    }


    wheelie::printer::PrinterManager::errPrinter.out = &std::cerr;
    wheelie::printer::PrinterManager::warnPrinter.out = &std::cout;
    wheelie::printer::PrinterManager::infoPrinter.out = &std::cout;

    std::string path = argv[1];
    ast::ASTNodeRoot *pRes = parser::parse(path);
    if (pRes) {
        auto stdPrinter = printer::PrinterManager::stdPrinter("|-> ", "|   ");
        pRes->print(stdPrinter);

        ElfWriter ew = ElfWriter();
        ew.write(pRes, "wheelcSlow");
        delete pRes;
    }


    return 0;
}