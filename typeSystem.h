#include <utility>

/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef BOOTSTRAP_TYPESYSTEM_H
#define BOOTSTRAP_TYPESYSTEM_H

#include <string>
#include <vector>
#include <map>
#include "types.h"

namespace wheelie { namespace parser { struct operatorData; }}
namespace wheelie { namespace printer { class Printer; }}
namespace wheelie { namespace ast { class ASTNodeRoot; }}
namespace wheelie { namespace ast { class ASTNode; }}
namespace wheelie {
    namespace typeSystem {

        struct typeDesc;

        struct typeData {
            u64 size;
            std::vector<std::pair<std::string, typeDesc>> fields;

            bool operator<(const typeData &other) const {
                return size < other.size;
            }
        };

        namespace builtin {
            extern typeData voidType;
            extern typeData unsigned8;
            extern typeData unsigned16;
            extern typeData unsigned32;
            extern typeData unsigned64;
            extern typeData signed8;
            extern typeData signed16;
            extern typeData signed32;
            extern typeData signed64;
            extern typeData funcType;
            extern typeData generalArray;
            extern typeData pointer;

            extern typeDesc constantTypeDesc;

            extern parser::operatorData addNum;
            extern parser::operatorData subNum;
            extern parser::operatorData negNum;
            extern parser::operatorData mulNum;
            extern parser::operatorData divNum;
            extern parser::operatorData modNum;
            extern parser::operatorData orNum;
            extern parser::operatorData andNum;
            extern parser::operatorData xorNum;
            extern parser::operatorData notNum;
            extern parser::operatorData shlNum;
            extern parser::operatorData shrNum;
            extern parser::operatorData arrIndex;
        }

        struct typeDesc {
            std::string name;
            typeData *resolved{};
            locationData locData;
            typeDesc *arrayDesc{};
            s64 elements{-1};
            std::vector<typeDesc> genericInst;
            s32 genericIndex{-1};

            typeDesc(const typeDesc &old) : name(old.name), resolved(old.resolved), locData(old.locData),
                                            elements(old.elements), genericInst(old.genericInst),
                                            genericIndex(old.genericIndex) {
                if (old.arrayDesc)
                    arrayDesc = new typeDesc(*old.arrayDesc);
            }

            typeDesc(typeDesc &&old) noexcept : name(std::move(old.name)), resolved(old.resolved), locData(old.locData),
                                                arrayDesc(old.arrayDesc), elements(old.elements),
                                                genericInst(std::move(old.genericInst)),
                                                genericIndex(old.genericIndex) {
                old.arrayDesc = nullptr;
            }

            typeDesc &operator=(typeDesc const &old) {
                name = old.name;
                resolved = old.resolved;
                locData = old.locData;
                delete arrayDesc;
                if (old.arrayDesc)
                    arrayDesc = new typeDesc(*old.arrayDesc);
                else
                    arrayDesc = nullptr;
                elements = old.elements;
                genericInst = old.genericInst;
                genericIndex = old.genericIndex;
                return *this;
            }

            typeDesc(std::string name, const locationData &locData) : name(std::move(name)), locData(locData) {}

            typeDesc(std::string name, typeData *data) : name(std::move(name)), resolved(data), locData("") {}

            explicit typeDesc(int genericIndex) : name(""), locData(""), genericIndex(genericIndex) {}

            explicit typeDesc(int genericIndex, int elems) : name(""), locData(""),
                                                             elements(elems) {
                if (elements != -1) {
                    resolved = &builtin::generalArray;
                } else {
                    resolved = &builtin::pointer;
                }
                arrayDesc = new typeDesc(genericIndex);
            }

            typeDesc() : name("void"), resolved(&builtin::voidType), locData("") {}

            ~typeDesc() {
                delete arrayDesc;
            }

            void print(printer::Printer &p) const;

            u64 getSize() const {
                if (!resolved) return 0;
                if (arrayDesc) {
                    if (elements != -1) return resolved->size * elements;
                    else return 16;
                } else return resolved->size;
            }
        };

        extern std::map<std::pair<typeData, typeData>, bool> convertibilityMap;

        extern void prepareTypes(ast::ASTNodeRoot *root);

        extern bool buildTypesAndResolve(ast::ASTNodeRoot *root);

        extern typeDesc *getType(ast::ASTNode *node);

    }
}
#endif //BOOTSTRAP_TYPESYSTEM_H
