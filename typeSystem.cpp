/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "typeSystem.h"
#include "ast.h"
#include "printer.h"

namespace wheelie {
    namespace typeSystem {

        namespace builtin {
            typeData voidType{0};
            typeData funcType{8};
            typeData unsigned8{1};
            typeData unsigned16{2};
            typeData unsigned32{4};
            typeData unsigned64{8};
            typeData signed8{1};
            typeData signed16{2};
            typeData signed32{4};
            typeData signed64{8};
            typeData generalArray{16};
            typeData pointer{8};

            typeDesc constantTypeDesc("s64", &signed64);

            parser::operatorData addNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData subNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData negNum(new typeDesc(constantTypeDesc), nullptr, new typeDesc(constantTypeDesc));
            parser::operatorData mulNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData divNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData modNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData orNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                       new typeDesc(constantTypeDesc));
            parser::operatorData andNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData xorNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData notNum(new typeDesc(constantTypeDesc), nullptr, new typeDesc(constantTypeDesc));
            parser::operatorData shlNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData shrNum(new typeDesc(constantTypeDesc), new typeDesc(constantTypeDesc),
                                        new typeDesc(constantTypeDesc));
            parser::operatorData arrIndex(new typeDesc(0), new typeDesc(0, -1), new typeDesc(constantTypeDesc));
        }

        std::map<std::pair<typeData, typeData>, bool> convertibilityMap;

        void prepareTypes(ast::ASTNodeRoot *root) {
            // Built-in types
            root->types["u8"] = &builtin::unsigned8;
            root->types["u16"] = &builtin::unsigned16;
            root->types["u32"] = &builtin::unsigned32;
            root->types["u64"] = &builtin::unsigned64;
            root->types["s8"] = &builtin::signed8;
            root->types["s16"] = &builtin::signed16;
            root->types["s32"] = &builtin::signed32;
            root->types["s64"] = &builtin::signed64;
            builtin::generalArray.fields.emplace_back("<{DUMMY}>", typeDesc("u64", &builtin::unsigned64));
            builtin::generalArray.fields.emplace_back("length", typeDesc("u64", &builtin::unsigned64));

#define ADD_CONV(t1, t2) convertibilityMap[std::make_pair(builtin::t1, builtin::t2)] = true
#define ADD_CONV_N(i1, i2) ADD_CONV(unsigned##i1, unsigned##i2);ADD_CONV(signed##i1, unsigned##i2);ADD_CONV(unsigned##i1, signed##i2);ADD_CONV(signed##i1, signed##i2)

            ADD_CONV_N(8, 8);
            ADD_CONV_N(8, 16);
            ADD_CONV_N(8, 32);
            ADD_CONV_N(8, 64);
            ADD_CONV_N(16, 16);
            ADD_CONV_N(16, 32);
            ADD_CONV_N(16, 64);
            ADD_CONV_N(32, 32);
            ADD_CONV_N(32, 64);
            ADD_CONV_N(64, 64);

#undef ADD_CONV_N
#undef ADD_CONV

            root->operators["if+"] = &builtin::addNum;
            root->operators["if-"] = &builtin::subNum;
            root->operators["pr-"] = &builtin::negNum;
            root->operators["if*"] = &builtin::mulNum;
            root->operators["if/"] = &builtin::divNum;
            root->operators["if%"] = &builtin::modNum;
            root->operators["if|"] = &builtin::orNum;
            root->operators["if&"] = &builtin::andNum;
            root->operators["if^"] = &builtin::xorNum;
            root->operators["pr~"] = &builtin::notNum;
            root->operators["if<<"] = &builtin::shlNum;
            root->operators["if>>"] = &builtin::shrNum;
            root->operators["if[]"] = &builtin::arrIndex;
        }

        typeDesc *getType(ast::ASTNode *node) {
            switch (node->type) {
                case ast::None:
                    break;
                case ast::Root:
                    break;
                case ast::Function: {
                    auto *n2 = dynamic_cast<ast::ASTNodeFunction *>(node);
                    return n2->retType;
                }
                case ast::Return:
                    break;
                case ast::Wide:
                    break;
                case ast::Operator: {
                    auto *n2 = dynamic_cast<ast::ASTNodeOperator *>(node);
                    return &n2->retType;
                }
                case ast::NumConstant: {
                    return &builtin::constantTypeDesc;
                }
                case ast::SymbolUse: {
                    auto *n2 = dynamic_cast<ast::ASTNodeSymbolUse *>(node);
                    return n2->resolved->type;
                }
                case ast::Declaration:
                    break;
                case ast::Assignment:
                    break;
                case ast::Dot: {
                    auto *n2 = dynamic_cast<ast::ASTNodeDot *>(node);
                    return n2->resolvedType;
                }
            }
            return nullptr;
        }

        bool resolveGenericType(ast::ASTNodeRoot *root, typeDesc *type, ast::ASTNode *parent) {
            if (type->genericIndex == -1) return true;
            switch (parent->type) {
                case ast::Operator: {
                    auto *node = dynamic_cast<ast::ASTNodeOperator *>(parent);
                    if (node->left) {
                        auto *leftType = getType(node->left);
                        if ((u32) type->genericIndex < leftType->genericInst.size()) {
                            *type = leftType->genericInst[type->genericIndex];
                            break;
                        }
                        if (node->right)
                            type->genericIndex -= leftType->genericInst.size();
                    }
                    if (node->right) {
                        auto *rightType = getType(node->right);
                        if ((u32) type->genericIndex < rightType->genericInst.size()) {
                            *type = rightType->genericInst[type->genericIndex];
                            break;
                        }
                    }
                    return false;
                }
                case ast::None:
                    break;
                case ast::Root:
                    break;
                case ast::Function:
                    break;
                case ast::Return:
                    break;
                case ast::Wide:
                    break;
                case ast::NumConstant:
                    break;
                case ast::SymbolUse:
                    break;
                case ast::Declaration:
                    break;
                case ast::Assignment:
                    break;
                case ast::Dot:
                    break;
            }
            return true;
        }

        static typeDesc tmpDesc{};

        bool isTypeConvertible(typeDesc *from, typeDesc *to, ast::ASTNodeRoot *root, ast::ASTNode *genericResolver) {
            if (!from || !to) return false;
            if (from->genericIndex != -1) {
                tmpDesc = *from;
                if (!resolveGenericType(root, &tmpDesc, genericResolver)) return false;
                return isTypeConvertible(&tmpDesc, to, root, genericResolver);
            }
            if (to->genericIndex != -1) {
                tmpDesc = *to;
                if (!resolveGenericType(root, &tmpDesc, genericResolver)) return false;
                return isTypeConvertible(from, &tmpDesc, root, genericResolver);
            }
            if (from->elements != to->elements) return false;
            if ((from->arrayDesc && !to->arrayDesc) || (to->arrayDesc && !from->arrayDesc)) return false;
            if (from->arrayDesc) return isTypeConvertible(from->arrayDesc, to->arrayDesc, root, genericResolver);
            if (from->resolved == to->resolved) return true;
            auto it = convertibilityMap.find(std::make_pair(*from->resolved, *to->resolved));
            return it != convertibilityMap.end();
        }

        bool resolveType(ast::ASTNodeRoot *root, typeDesc *type, ast::ASTNode *parent) {
            if (type->arrayDesc && !resolveType(root, type->arrayDesc, parent)) return false;
            if (!resolveGenericType(root, type, parent)) return false;
            for (auto &it : type->genericInst) {
                if (!resolveType(root, &it, parent)) return false;
            }
            if (type->resolved) return true;
            auto iterType = root->types.find(type->name);
            if (iterType == root->types.end()) {
                const locationData &ld = type->locData;
                printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: Could not resolve type '%s'",
                                                            ld.file.c_str(), ld.line, ld.column,
                                                            type->name.c_str());
                return false;
            } else {
                typeData *td = iterType->second;
                type->resolved = td;
            }
            return true;
        }

        bool resolveMemberOffset(ast::ASTNodeDot *nodeDot) {
            typeDesc *parType = nullptr;
            switch (nodeDot->parent->type) {
                case ast::Dot: {
                    auto *asDot = dynamic_cast<ast::ASTNodeDot *>(nodeDot->parent);
                    parType = asDot->resolvedType;
                    break;
                }
                case ast::SymbolUse: {
                    auto *asSymUse = dynamic_cast<ast::ASTNodeSymbolUse *>(nodeDot->parent);
                    parType = asSymUse->resolved->type;
                    break;
                }
                case ast::None:
                    break;
                case ast::Root:
                    break;
                case ast::Function:
                    break;
                case ast::Return:
                    break;
                case ast::Wide:
                    break;
                case ast::Operator: {
                    auto *asOp = dynamic_cast<ast::ASTNodeOperator *>(nodeDot->parent);
                    parType = &asOp->retType;
                    break;
                }
                case ast::NumConstant:
                    break;
                case ast::Declaration:
                    break;
                case ast::Assignment:
                    break;
            }
            int offset = 0;
            for (std::pair<std::string, typeDesc> &member : parType->resolved->fields) {
                if (member.first == nodeDot->member.value) {
                    nodeDot->resolvedType = &member.second;
                    nodeDot->offset = offset;
                    return true;
                }
                offset += member.second.getSize();
            }
            const locationData &ld = nodeDot->member.locData;
            printer::PrinterManager::errPrinter.print("%s:%d:%d: error: Could not resolve member '%s' of type '",
                                                      ld.file.c_str(), ld.line, ld.column,
                                                      nodeDot->member.value.c_str());
            parType->print(printer::PrinterManager::errPrinter);
            printer::PrinterManager::errPrinter.printLn("'");
            return false;
        }

        typedef std::vector<std::map<std::string, const parser::declData *>> scopeListType;

        const parser::declData *resolveSymbolFromScope(scopeListType &scopeList, const parser::Token &symbol) {
            for (auto it = scopeList.rbegin(); it != scopeList.rend(); it++) {
                auto sym = it->find(symbol.value);
                if (sym != it->end()) {
                    return sym->second;
                }
            }
            const locationData &ld = symbol.locData;
            printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: Could not resolve symbol '%s'",
                                                        ld.file.c_str(), ld.line, ld.column,
                                                        symbol.value.c_str());
            return nullptr;
        }

        bool resolve(ast::ASTNodeRoot *root, scopeListType &scopeList, ast::ASTNode *cur) {
            bool ret = true;
            switch (cur->type) {
                case ast::Function: {
                    auto *nodeFunc = dynamic_cast<ast::ASTNodeFunction *>(cur);
                    scopeList.back().operator[](nodeFunc->name.value) = &nodeFunc->declSelf;
                    scopeList.emplace_back();
                    for (auto &arg : nodeFunc->args) {
                        ret &= resolveType(root, arg.type, cur);
                        scopeList.back().operator[](arg.name.value) = &arg;
                    }
                    ret &= resolveType(root, nodeFunc->retType, cur);
                    for (ast::ASTNode *child : nodeFunc->children) {
                        ret &= resolve(root, scopeList, child);
                    }
                    break;
                }
                case ast::Wide: {
                    auto *nodeWide = dynamic_cast<ast::ASTNodeWide *>(cur);
                    for (ast::ASTNode *child : nodeWide->children) {
                        ret &= resolve(root, scopeList, child);
                    }
                    break;
                }
                case ast::Root: {
                    auto *nodeRoot = dynamic_cast<ast::ASTNodeRoot *>(cur);
                    for (ast::ASTNode *child : nodeRoot->children) {
                        ret &= resolve(root, scopeList, child);
                    }
                    break;
                }
                case ast::Operator: {
                    auto *nodeOperator = dynamic_cast<ast::ASTNodeOperator *>(cur);
                    if (nodeOperator->left) ret &= resolve(root, scopeList, nodeOperator->left);
                    if (nodeOperator->right) ret &= resolve(root, scopeList, nodeOperator->right);
                    if (!ret) return false;
                    std::string toSearch = ((nodeOperator->left && nodeOperator->right) ? "if" : (nodeOperator->left
                                                                                                  ? "po"
                                                                                                  : "pr"));
                    toSearch += nodeOperator->op;
                    typeDesc *leftType = nullptr, *rightType = nullptr;
                    auto it = root->operators.find(toSearch);
                    if (it == root->operators.end()) {
                        ret = false;
                    } else {
                        nodeOperator->resolved = it->second;
                        if (nodeOperator->left) {
                            leftType = getType(nodeOperator->left);
                            if (!isTypeConvertible(leftType, nodeOperator->resolved->leftType, root, cur))
                                ret = false;
                        } else if (nodeOperator->resolved->leftType) ret = false;

                        if (nodeOperator->right) {
                            rightType = getType(nodeOperator->right);
                            if (!isTypeConvertible(rightType, nodeOperator->resolved->rightType, root, cur))
                                ret = false;
                        } else if (nodeOperator->resolved->rightType) ret = false;
                    }

                    if (ret) {
                        nodeOperator->retType = *nodeOperator->resolved->returnType;
                        ret &= resolveType(root, &nodeOperator->retType, cur);
                    } else {
                        const locationData &ld = nodeOperator->locData;
                        printer::PrinterManager::errPrinter.print("%s:%d:%d: error: Could not resolve '%s operator%s(",
                                                                  ld.file.c_str(), ld.line, ld.column,
                                                                  (!leftType ? "prefix" : (!rightType ? "postfix"
                                                                                                      : "infix")),
                                                                  nodeOperator->op.c_str());
                        if (leftType) {
                            leftType->print(printer::PrinterManager::errPrinter);
                            if (rightType)
                                printer::PrinterManager::errPrinter.print(", ");
                        }
                        if (rightType) {
                            rightType->print(printer::PrinterManager::errPrinter);
                        }
                        //parType->print(PrinterManager::errPrinter);
                        printer::PrinterManager::errPrinter.printLn(")'");
                        return false;
                    }
                    break;
                }
                case ast::Return: {
                    auto *nodeRet = dynamic_cast<ast::ASTNodeReturn *>(cur);
                    ret &= resolve(root, scopeList, nodeRet->returnValue);
                    break;
                }
                case ast::SymbolUse: {
                    auto *nodeSymb = dynamic_cast<ast::ASTNodeSymbolUse *>(cur);
                    if (!nodeSymb->resolved) {
                        nodeSymb->resolved = resolveSymbolFromScope(scopeList, nodeSymb->name);
                        ret &= (nodeSymb->resolved != nullptr);
                    }
                    break;
                }
                case ast::Declaration: {
                    auto *nodeDecl = dynamic_cast<ast::ASTNodeDeclaration *>(cur);
                    if (!nodeDecl->data.type->resolved) {
                        ret &= resolveType(root, nodeDecl->data.type, cur);
                    }
                    if (nodeDecl->value) ret &= resolve(root, scopeList, nodeDecl->value);
                    scopeList.back().operator[](nodeDecl->data.name.value) = &nodeDecl->data;
                    break;
                }
                case ast::Assignment: {
                    auto *nodeAssignment = dynamic_cast<ast::ASTNodeAssignment *>(cur);
                    if (!nodeAssignment->resolved) {
                        nodeAssignment->resolved = resolveSymbolFromScope(scopeList, nodeAssignment->name);
                        ret &= (nodeAssignment->resolved != nullptr);
                    }
                    if (nodeAssignment->value) ret &= resolve(root, scopeList, nodeAssignment->value);
                    break;
                }
                case ast::Dot: {
                    auto *nodeDot = dynamic_cast<ast::ASTNodeDot *>(cur);
                    ret &= resolve(root, scopeList, nodeDot->parent);
                    if (ret) {
                        ret &= resolveMemberOffset(nodeDot);
                    }
                }
                case ast::NumConstant:
                case ast::None:
                    break;
            }
            return ret;
        }

        bool buildTypesAndResolve(ast::ASTNodeRoot *root) {
            std::vector<ast::ASTNode *> todo;
            scopeListType scopeList; //TODO: More robust way with visibility
            scopeList.emplace_back();
            //TODO: Build list of global symbols and types

            return resolve(root, scopeList, root);
        }

        void typeDesc::print(printer::Printer &p) const {
            p.print("%s", name.c_str());
            const typeDesc *next = this;
            while (next->arrayDesc) {
                if (next->elements != -1)
                    p.print("[%d]", next->elements);
                else
                    p.print("[]");
                next = next->arrayDesc;
            }
        }
    }
}
