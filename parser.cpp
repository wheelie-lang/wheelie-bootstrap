/*
    Copyright 2019 Arno Hemelhof

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include "parser.h"

#include <fstream>

#include "ast.h"
#include "printer.h"
#include "typeSystem.h"

namespace wheelie {
    namespace parser {
        u32 parserError = 0;

        u64 parseNumber(const Token &t, bool *pIsFloat = nullptr) {
            union {
                u64 integ;
                double flt;
            } ret{};

            bool isFloat = t.metadata & 1u;
            u8 radix = t.metadata >> 1u;
            if (isFloat) {
            } else {
                for (char c : t.value) {
                    if (c == '\'') continue;
                    c = toupper(c);
                    int64_t val = c - '0';
                    if (val > 9) val = c - 'A' + 10;
                    if (val < 0 || val >= radix) {
                        printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: invalid base-%d literal '%s'",
                                                                    t.locData.file.c_str(), t.locData.line,
                                                                    t.locData.column, radix,
                                                                    t.value.c_str());
                        parserError |= 4u;
                        return 0;
                    }
                    ret.integ *= (u64) radix;
                    ret.integ += (u64) val;
                }
            }

            if (pIsFloat) *pIsFloat = isFloat;
            return ret.integ;
        }

        void operator+=(Token &t1, const Token &t2) {
            t1.value += t2.value;
        }

        static bool isDelim(const u8 c) {
            switch (c) {
                case '\t':
                case '\r':
                case '\f':
                case '\n':
                case '\v':
                case ' ':
                case '(':
                case '{':
                case '[':
                case ']':
                case '}':
                case ')':
                case ':':
                case ';':
                case '\'':
                case '"':
                    return true;
                default:
                    return false;
            }
        }

        static bool isSymStart(const u8 c) {
            return !isDelim(c) && (isalpha(c) || c == '_');
        }

        static bool isSymChar(const u8 c) {
            return isSymStart(c) || isalnum(c);
        }

        class Tokenizer {
            char *text;
            char *indexed;
            s64 length;
            u32 line{1}, column{1};
            Token *token;
            char textMode = 0;
            bool done = false;
        public:
            const std::string file;
            bool valid = false;

            explicit Tokenizer(const std::string &path) : file(path) {
                printer::PrinterManager::files.push_back(path);
                auto ifs = std::ifstream(path);
                if (ifs.is_open()) {
                    auto begin = ifs.tellg();
                    ifs.seekg(0, std::ios::end);
                    length = ifs.tellg() - begin;
                    if (length < 0) {
                        printer::PrinterManager::errPrinter.print("[\"parser.cpp\" %s line %d] Internal Error!",
                                                                  __FUNCTION__,
                                                                  __LINE__);
                        text = nullptr;
                        return;
                    }

                    text = new char[length + 1];
                    indexed = text;
                    ifs.seekg(0, std::ios::beg);
                    ifs.read(text, length);

                    valid = true;
                    ifs.close();

                    token = new Token(printer::PrinterManager::files.back());

                    this->consume();
                } else {
                    printer::PrinterManager::errPrinter.print("Tried to import file \"%s\", but it wasn't found!\n",
                                                              path.c_str());
                }
            }

            bool hasNext() {
                return !done && !parserError;
            }

            void consume() {
                char *start = indexed;
                if (textMode) {
                    token->locData.line = line;
                    token->locData.column = column;
                    char *end = start;
                    for (; end < (text + length) && *end != '"'; end++, column++) {
                        if (*end == '\\' && (end + 1) < (text + length)) {
                            //TODO: Implement escape sequences
                        }
                    }
                    token->value = std::string(start, end);
                    token->tag = Text;
                    indexed = end;
                    return;
                }
                for (; start < (text + length); start++, column++) {
                    u8 c = static_cast<u8>(*start);
                    if (c == '\n') {
                        line++;
                        column = 0;
                    } else if (!std::isspace(c)) {
                        break;
                    }
                }
                if (start >= (text + length)) {
                    indexed = start;
                    done = true;
                    token->value = "";
                    return;
                }
                u8 c = static_cast<u8>(*start);
                token->locData.line = line;
                token->locData.column = column;
                token->tag = General;
                char *end = start + 1;
                column++;
                if (std::isdigit(c)) {
                    u8 radix = 10;
                    if (end < (text + length) && c == '0') {
                        switch (std::tolower(static_cast<u8>(*end))) {
                            case 'b':
                                radix = 2;
                                break;
                            case 'x':
                                radix = 16;
                                break;
                            case 'd':
                                break;
                            case 'o':
                                radix = 8;
                                break;
                            default:
                                start -= 2;
                                end--;
                                column--;
                                break;
                        }
                        start += 2;
                        end++;
                        column++;
                    }
                    bool isFloat = false;
                    for (; end < (text + length) &&
                           (std::isalnum(static_cast<u8>(*end)) || *end == '\''); end++, column++) {}
                    if (end < (text + length) && *end == '.') {
                        isFloat = true;
                        end++;
                        for (; end < (text + length) &&
                               (std::isalnum(static_cast<u8>(*end)) || *end == '\''); end++, column++) {}
                    }
                    token->tag = Number;
                    token->metadata = ((u8) (radix << 1u)) | ((u8) isFloat);
                } else if (c == '"' || c == '\'') {
                    textMode = textMode ? (char) 0 : c;
                } else if (isDelim(c)) {
                    token->tag = Delimiter;
                } else if (!isSymStart(c)) {
                    token->tag = TokenTag::Operator;
                    for (; end < (text + length) && !isSymChar(static_cast<u8>(*end)) &&
                           !isDelim(static_cast<u8>(*end)); end++, column++) {}
                } else {
                    for (; end < (text + length) && isSymChar(static_cast<u8>(*end)); end++, column++) {}
                }
                indexed = end;
                token->value = std::string(start, end);
            }

            const Token &peek() {
                if (!hasNext() && token->value.empty()) {
                    parserError |= 1u;
                    printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: Unexpected EOF",
                                                                token->locData.file.c_str(),
                                                                token->locData.line, token->locData.column);
                }
                return *token;
            }

            Token next() {
                Token ret = peek();
                consume();
                return ret;
            }

            bool match(const char *pattern) {
                if (peek().value == pattern) {
                    consume();
                    return true;
                }
                return false;
            }

            void consumeOptional(const char *pattern) {
                match(pattern);
            }

            bool assert(const char *pattern) {
                if (!match(pattern)) {
                    parserError |= 2u;
                    printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: Expected '%s', but found '%s'",
                                                                token->locData.file.c_str(), token->locData.line,
                                                                token->locData.column, pattern, token->value.c_str());
                    return false;
                }
                return true;
            }

            ~Tokenizer() {
                delete text;
                delete token;
            }
        };

        static void parseBlock(Tokenizer &tokenizer, std::vector<ast::ASTNode *> &out);

        static typeSystem::typeDesc *parseType(Tokenizer &tokenizer) {
            Token typeName = tokenizer.next();
            typeSystem::typeDesc *ret = new typeSystem::typeDesc(typeName.value, typeName.locData);
            Token next = tokenizer.peek();
            typeSystem::typeDesc *toEdit = ret;
            typeSystem::typeDesc *prev = nullptr;
            while (next.value == "[") {
                toEdit->arrayDesc = new typeSystem::typeDesc(typeName.value, typeName.locData);
                tokenizer.consume();
                next = tokenizer.peek();
                if (next.value != "]") {
                    //TODO: add support for expressions here
                    bool isFloat = false;
                    u64 elemCount = parseNumber(next, &isFloat);
                    toEdit->elements = elemCount;
                    tokenizer.consume();
                    toEdit->resolved = &typeSystem::builtin::pointer;
                } else {
                    toEdit->resolved = &typeSystem::builtin::generalArray;
                }
                tokenizer.assert("]");
                toEdit->genericInst.emplace_back(*toEdit->arrayDesc);
                if (prev) prev->genericInst[0] = *toEdit;
                prev = toEdit;
                toEdit = toEdit->arrayDesc;
                next = tokenizer.peek();
            }
            return ret;
        }

        static ast::ASTNodeFunction *parseFunction(Tokenizer &tokenizer) {
            tokenizer.consume();
            auto *ret = new ast::ASTNodeFunction(tokenizer.next());

            if (tokenizer.match("(")) {
                while (tokenizer.hasNext() && !tokenizer.match(")")) {
                    Token name = tokenizer.next();
                    tokenizer.assert(":");
                    ret->args.emplace_back(parseType(tokenizer), name, true);
                    tokenizer.consumeOptional(",");
                }
            }

            if (tokenizer.match(":")) {
                ret->retType = parseType(tokenizer);
            }

            if (tokenizer.assert("{")) {
                parseBlock(tokenizer, ret->children);
            }

            return ret;
        }

        static ast::ASTNode *
        parseExpression(Tokenizer &tokenizer, bool inFunction = true, bool allowEmpty = false, bool isPrimary = false) {
            Token t = tokenizer.peek();
            ast::ASTNode *ret = nullptr;
            if (t.value == "func") {
                return parseFunction(tokenizer);
            } else if (t.value == "return") {
                tokenizer.consume();
                auto *retR = new ast::ASTNodeReturn();
                if (!tokenizer.match(";")) {
                    retR->returnValue = parseExpression(tokenizer);
                    tokenizer.assert(";");
                }
                return retR;
            } else if (t.tag == Number) {
                bool isFloat = false;
                u64 num = parseNumber(tokenizer.next(), &isFloat);
                ret = new ast::ASTNodeNumber(num, isFloat);
            } else if (t.tag == General) {
                tokenizer.consume();
                if (tokenizer.match(":")) {
                    auto *decl = new ast::ASTNodeDeclaration(t, parseType(tokenizer), true);
                    if (tokenizer.match("=")) {
                        decl->value = parseExpression(tokenizer, inFunction);
                    }
                    tokenizer.assert(";");
                    return decl;
                } else if (tokenizer.match("=")) {
                    ret = new ast::ASTNodeAssignment(t, parseExpression(tokenizer, inFunction));
                } else {
                    ret = new ast::ASTNodeSymbolUse(t);
                }
            } else if (t.tag == TokenTag::Operator) {
                auto *retOp = new ast::ASTNodeOperator(tokenizer.next());
                retOp->right = parseExpression(tokenizer);
                if (retOp->right && retOp->right->type == ast::NumConstant && retOp->op == "-") {
                    // This is actually just a negative number
                    auto *num = dynamic_cast<ast::ASTNodeNumber *>(retOp->right);
                    retOp->right = nullptr;
                    delete retOp;
                    if (num->isFloat) num->value.floating = -num->value.floating;
                    else num->value.integral = -num->value.integral;
                    return num;
                }
                return retOp;
            } else if (t.tag == Delimiter && t.value == ";") {
                if (allowEmpty) {
                    do {
                        tokenizer.consume();
                        t = tokenizer.peek();
                    } while (t.tag == Delimiter && t.value == ";");
                } else {
                    printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: Expected an expression",
                                                                t.locData.file.c_str(), t.locData.line,
                                                                t.locData.column);
                    tokenizer.consume();
                    parserError |= 4u;
                }
                return nullptr;
            } else {
                printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: Unexpected token '%s'",
                                                            t.locData.file.c_str(), t.locData.line, t.locData.column,
                                                            t.value.c_str());
                tokenizer.consume();
                parserError |= 4u;
                return nullptr;
            }
            auto nextToken = tokenizer.peek();
            while (nextToken.tag == TokenTag::Operator || nextToken.value == "[") {
                if (nextToken.value == "[") {
                    tokenizer.consume();
                    auto *op = new ast::ASTNodeOperator(nextToken);
                    op->left = ret;
                    op->right = parseExpression(tokenizer);
                    op->op = "[]";
                    op->resolved = &typeSystem::builtin::arrIndex;
                    tokenizer.assert("]");
                    ret = op;
                } else {
                    if (nextToken.value == ".") {
                        tokenizer.consume();
                        do {
                            nextToken = tokenizer.next();
                            if (nextToken.tag != General) {
                                auto &ld = nextToken.locData;
                                printer::PrinterManager::errPrinter.printLn("%s:%d:%d: error: Unexpected token '%s'",
                                                                            ld.file.c_str(), ld.line, ld.column,
                                                                            nextToken.value.c_str());
                                parserError |= 4u;
                                delete ret;
                                return nullptr;
                            }
                            ret = new ast::ASTNodeDot(ret, nextToken);
                        } while (tokenizer.match("."));
                    } else {
                        auto *retOp = new ast::ASTNodeOperator(nextToken);
                        retOp->left = ret;
                        tokenizer.consume();
                        if (tokenizer.peek().tag != Delimiter) {
                            retOp->right = parseExpression(tokenizer);
                        }
                        ret = retOp;
                    }
                }
                nextToken = tokenizer.peek();
            }
            return ret;
        }

        static void parseBlock(Tokenizer &tokenizer, std::vector<ast::ASTNode *> &out) {
            while (!tokenizer.match("}") && tokenizer.hasNext()) {
                ast::ASTNode *expr = parseExpression(tokenizer, true, true, true);
                if (expr)
                    out.push_back(expr);
            }
        }

        ast::ASTNodeRoot *parse(const std::string &path) {
            printer::PrinterManager::files.emplace_back("root");
            auto *root = new ast::ASTNodeRoot(0);
            auto *child = new ast::ASTNodeRoot(1); //TODO: Support multiple files ('@import' statement)
            root->children.push_back(child);

            auto tokenizer = Tokenizer(path);
            if (!tokenizer.valid) {
                delete root;
                return nullptr;
            }

            while (tokenizer.hasNext()) {
                ast::ASTNode *expr = parseExpression(tokenizer, false, true, true);
                if (expr)
                    child->children.push_back(expr);
            }

            if (parserError) {
                delete root;
                return nullptr;
            }

            typeSystem::prepareTypes(root);
            if (typeSystem::buildTypesAndResolve(root))
                return root;
            else
                return nullptr;
        }

    }
}
